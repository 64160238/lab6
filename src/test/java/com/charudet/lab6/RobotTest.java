package com.charudet.lab6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
	@Test
	public void shouldDownOver() {
		Robot robot = new Robot("Robot", 'R', 0, Robot.MAX_Y);
		assertEquals(false, robot.down());
		assertEquals(Robot.MAX_Y, robot.getY());
	}

	@Test
	public void shouldDownOver2() {
		Robot robot = new Robot("Robot", 'R');
	}
	
	@Test
	public void shouldUpNegative() {
		Robot robot = new Robot("Robot", 'R', 0, Robot.MIN_Y);
		assertEquals(false, robot.up());
		assertEquals(Robot.MIN_Y, robot.getY());
	}

	@Test
	public void shouldDownSuccess() {
		Robot robot = new Robot("Robot", 'R', 0, 0);
		assertEquals(true, robot.down());
		assertEquals(1, robot.getY());
	}

	@Test
	public void shouldDownNSuccess() {
		Robot robot = new Robot("Robot", 'R', 0, 10);
		assertEquals(true, robot.down(5));
		assertEquals(10, robot.getY());
	}

	@Test
	public void shouldUpSuccess() {
		Robot robot = new Robot("Robot", 'R', 0, 1);
		assertEquals(true, robot.up());
		assertEquals(0, robot.getY());
	}

	@Test
	public void shouldUpNSuccess() {
		Robot robot = new Robot("Robot", 'R', 10, 11);
		assertEquals(true, robot.up(5));
		assertEquals(6, robot.getY());
	}

	@Test
	public void shouldUpNSuccess2() {
		Robot robot = new Robot("Robot", 'R', 10, 11);
		assertEquals(true, robot.up(11));
		assertEquals(0, robot.getY());
	}

	@Test
	public void shouldLeft() {
		Robot robot = new Robot("Robot", 'R', 0, Robot.MAX_X);
		assertEquals(true, robot.left());
		assertEquals(-1, robot.getX());
	}

	@Test
	public void shouldLeft1() {
		Robot robot = new Robot("Robot", 'R', 5, Robot.MAX_X);
		assertEquals(true, robot.left(5));
		assertEquals(5, robot.getX());
	}

	@Test
	public void shouldRight() {
		Robot robot = new Robot("Robot", 'R', 0, Robot.MIN_Y);
		assertEquals(true, robot.right());
		assertEquals(1, robot.getX());
	}

	@Test
	public void shouldRight1() {
		Robot robot = new Robot("Robot", 'R', 1, Robot.MIN_Y);
		assertEquals(true, robot.right(1));
		assertEquals(1, robot.getX());
	}
}
