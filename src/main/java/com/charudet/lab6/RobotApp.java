package com.charudet.lab6;

public class RobotApp {
	public static void main(String[] args) {
		Robot kapong = new Robot("kapong", 'K', 0, 0);
		kapong.print();
		for (int i = 0; i < 20; i++) {
			kapong.right();
			kapong.down();
		}
		kapong.print();

		Robot blue = new Robot("Blue", 'B', 1, 1);
		blue.print();

		for (int y = Robot.MIN_Y; y <= Robot.MAX_Y; y++) {
			for (int x = Robot.MIN_X; x <= Robot.MAX_X; x++) {
				if (kapong.getX() == x && kapong.getY() == y) {
					System.out.print(kapong.getSymbol());
				} else if (blue.getX() == x && blue.getY() == y) {
					System.out.print(blue.getSymbol());
				} else {
					System.out.print("-");
				}
			}
			System.out.println();
		}
	}
}
