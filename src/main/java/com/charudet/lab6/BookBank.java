package com.charudet.lab6;

public class BookBank {
	// Attributes
	private String name;
	private double balance;

	public BookBank(String name, double balance) { // Constructor
		this.name = name;
		this.balance = balance;

	}

	// Methods
	public boolean deposit(double money) {
		if(money < 1) return false;
		balance = balance + money;
		return true;
	}

	public boolean whitdraw(double money) {
		if(money < 1) return false;
		if(money > balance) return false;
		balance = balance - money;
		return true;
	}

	public void print() {
		System.out.println(name + " " + balance);
	}

	public String getName() {
		return name;
	}

	public double getBalance() {
		return balance;
	}
}
