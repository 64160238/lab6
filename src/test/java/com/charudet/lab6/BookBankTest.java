package com.charudet.lab6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookBankTest {
	@Test
	public void shouldWithdrawSuccess() {
		BookBank book = new BookBank("Charudet", 20000);
		book.whitdraw(5000);
		assertEquals(15000, book.getBalance(), 0.00001);
	}

	@Test
	public void shouldWithdrawOverBalance() {
		BookBank book = new BookBank("Charudet", 20000);
		book.whitdraw(50000);
		assertEquals(20000, book.getBalance(), 0.00001);
	}

	@Test
	public void shouldWithdrawWithNegativeNumber() {
		BookBank book = new BookBank("Charudet", 20000);
		book.whitdraw(-20000);
		assertEquals(20000, book.getBalance(), 0.00001);
	}

	@Test
	public void shouldDepositSuccess() {
		BookBank book = new BookBank("Charudet", 20000);
		book.deposit(5000);
		assertEquals(25000, book.getBalance(), 0.00001);
	}
	
	@Test
	public void shouldDepositWithNegativeNumber() {
		BookBank book = new BookBank("Charudet", 20000);
		book.deposit(-20000);
		assertEquals(20000, book.getBalance(), 0.00001);
	}
}
